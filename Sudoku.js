
const TileImpl = {
    props: ['tile', 'showIncorrect'],
    template: `
        <div :class="classes" :style="style"
                @mousedown="$emit('select', this.tile)">
            <div>{{ this.tile.value }}</div>
        </div>
    `,
    data() {
        return {
            style: {
                "grid-column": this.tile.x,
                "grid-row": this.tile.y
            },
            fixedClasses: {
                tile: true,
                bottom: this.tile.y == 3 || this.tile.y == 6, 
                top: this.tile.y == 4 || this.tile.y == 7,
                right: this.tile.x == 3 || this.tile.x == 6,
                left: this.tile.x == 4 || this.tile.x == 7
            }
        };
    },
    computed: {
        classes() {
            return {
                ...this.fixedClasses,
                selected: this.tile.selected,
                fixed: this.tile.fixed,
                incorrect: this.showIncorrect && this.tile.incorrect
            };
        }
    }
};


const board = (function() {
    let returnee = [];

    // Populate the board with empty tiles
    // that have only their coordinate first.
    for (let y = 1; y <= 9; ++y)
    for (let x = 1; x <= 9; ++x) {
        let tile = {
            x: x,
            y: y,
            selected: false,
            fixed: false,
            incorrect: false,
            column: null,
            row: null,
            square: null,
            peers: null
        };
        returnee.push(tile);
    }

    // Fill the tile relationships.
    function sameColumn(tile1, tile2) {
        return tile1 != tile2 && tile1.x == tile2.x;
    }
    function sameRow(tile1, tile2) {
        return tile1 != tile2 && tile1.y == tile2.y;
    }
    function sameSquare(tile1, tile2) {
        let qx1 = Math.floor((tile1.x - 4) / 3);
        let qy1 = Math.floor((tile1.y - 4) / 3);
        let qx2 = Math.floor((tile2.x - 4) / 3);
        let qy2 = Math.floor((tile2.y - 4) / 3);
        return tile1 != tile2 && qx1 == qx2 && qy1 == qy2;
    }
    for (let tile of returnee) {
        tile.column = returnee.filter(t => sameColumn(t, tile));
        tile.row = returnee.filter(t => sameRow(t, tile));
        tile.square = returnee.filter(t => sameSquare(t, tile));
        tile.peers = [...tile.column, ...tile.row, ...tile.square];
    }

    // Now generate a board under the relationship constraints.
    function shuffle(array) {
        function random(min, max) {
            return min + Math.floor(Math.random() * (max - min));
        }
        for (let o = 0; o < array.length; ++o) {
            let next = random(o, array.length);
            let temp = array[next];
            array[next] = array[o];
            array[o] = temp;
        }
        return array;
    }
    function attemptLinearFill()
    {
        for (let tile of returnee) {
            let valid = n => !tile.peers.some(t => t.value == n);
            let candidates = shuffle([1, 2, 3, 4, 5, 6, 7, 8, 9]);        
            tile.value = candidates.find(valid);
            if (!tile.value) return false;
            tile.fixed = true;
        }
        return true;
    }
    while (!attemptLinearFill());
    let deletees = shuffle(Array.from(returnee)).slice(0, 40);
    for (let deletee of deletees) {
        deletee.value = null;
        deletee.fixed = false;
    }

    return returnee;
})();


const BoardImpl = {
    template: `
        <div class="boardwrapper"
                @keydown="keydown"
                @mousemove="mousemove"
                @mouseup="mouseup"
                @mouseleave="mouseup">
            <main class="board" tabindex="0">
                <Tile v-for="tile in board"
                        :tile="tile"
                        :showIncorrect="showIncorrect"
                        @select="select"
                        @mousedown="mousedown">
                </Tile>
            </main>
        </div>
    `,
    data() {
        return {
            board: board,
            selectedTile: null,
            dragging: false,
            dragX: null,
            dragY: null,
            showIncorrect: false
        };
    },
    methods: {
        select(tile) {
            if (tile.fixed) return;
            if (this.selectedTile) {
                this.selectedTile.selected = false;
            }
            this.selectedTile = tile;
            this.selectedTile.selected = true;
        },
        setValue(tile, value) {
            tile.value = value;
            let incorrect = function(t1) {
                if (t1.value == null) return false;
                return t1.peers.some(t2 => t1.value == t2.value);
            }
            tile.incorrect = incorrect(tile);
            for (let t of tile.peers) t.incorrect = incorrect(t);
        },
        keydown(event) {
            if (event.key == "h") {
                this.showIncorrect = !this.showIncorrect;
            }
            else {
                if (!this.selectedTile) return;
                const ALLOWED = [
                    "1", "2", "3", "4", "5",
                    "6", "7", "8", "9", "0"
                ];
                if (!ALLOWED.includes(event.key)) return;
                let value = event.key == "0" ? null : event.key;
                this.setValue(this.selectedTile, value);
            }
        },
        mousedown(event) {
            if (!this.selectedTile) return;
            this.dragging = true;
            this.dragX = event.x;
            this.dragY = event.y;
        },
        mouseup(event) {
            this.dragging = false;
            this.dragX = null;
            this.dragY = null;
        },
        mousemove(event) {
            if (!this.dragging) return;

            let dx = event.x - this.dragX;
            let dy = event.y - this.dragY;
            let s = window.innerHeight / 20;
            dx = Math.floor(dx / s);
            dy = Math.floor(dy / s);
            
            let value = 5 + dx - dy;
            if (value < 1) value = 1;
            if (value > 9) value = 9;
            this.setValue(this.selectedTile, value);
        },
    }
}


const app = Vue.createApp({});
app.component('Board', BoardImpl);
app.component('Tile', TileImpl);
app.mount(document.body.firstElementChild);

